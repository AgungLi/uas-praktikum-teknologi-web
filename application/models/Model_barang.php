<?php

class Model_barang extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    // Listing all produk home
    public function home()
    {
        $this->db->select('*');
        $this->db->from('barang');
        $this->db->limit(8);
        $query = $this->db->get();
        return $query->result();
    }

    // detail produk
    public function detail($id_barang)
    {
        $this->db->select('*');
        $this->db->from('barang');
        $this->db->where('id_barang', $id_barang);
        $this->db->order_by('id_barang', 'desc');
        $query = $this->db->get();
        return $query->row();
    }


    public function tampil_data()
    {
        $this->db->select('*');
        $this->db->from('barang');
        $query = $this->db->get();
        return $query->result();
    }

    //tambah
    public function tambah($data)
    {
        $this->db->insert('barang', $data);
    }

    //  edit
    public function edit($data)
    {
        $this->db->where('id_barang', $data['id_barang']);
        $this->db->update('barang', $data);
    }

    //  delete
    public function delete($data)
    {
        $this->db->where('id_barang', $data['id_barang']);
        $this->db->delete('barang', $data);
    }
}
