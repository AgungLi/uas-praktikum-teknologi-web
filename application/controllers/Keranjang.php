<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Keranjang extends CI_Controller
{
    // load model
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_barang');
        // load helper random stringg

        $this->load->helper('string');
    }
    // halaman belanja
    public function index()
    {
        $keranjang = $this->cart->contents();
        $data = array(
            'title'         => 'Keranjang Belanja',
            'keranjang'     => $keranjang,
            'isi'           => 'keranjang/list'
        );
        $this->load->view('layout/wrapper', $data, FALSE);
    }

    // Tambahkan ke keranjang belanja
    public function add()
    {
        // Ambil data dari form
        $id             = $this->input->post('id_barang');
        $qty            = $this->input->post('jum_barang');
        $price          = $this->input->post('harga_per_pcs');
        $name           = $this->input->post('nama_barang');
        $redirect_page  = $this->input->post('redirect_page');

        // Proses memasukan ke keranjang belanja
        $data = array(
            'id'      => $id,
            'qty'     => $qty,
            'price'   => $price,
            'name'    => $name
        );
        $this->cart->insert($data);
        // redirect page
        redirect($redirect_page, 'refresh');
    }

    // update cart
    public function update_cart($rowid)
    {
        //jika ada rowid
        if ($rowid) {
            $data = array(
                'rowid'     => $rowid,
                'qty'       => $this->input->post('jum_barang')
            );
            $this->cart->update($data);
            $this->session->set_flashdata('sukses', 'Data Keranjang telah diupdate');
            redirect(site_url('keranjang'), 'refresh');
        } else {
            // jika tidak ada rowid
            redirect(site_url('keranjang'), 'refresh');
        }
    }

    // Hapus semua isi keranjang belanjan
    public function hapus($rowid = '')
    {
        if ($rowid) {
            //hapus peritem
            $this->cart->remove($rowid);
            $this->session->set_flashdata('sukses', 'Data keranjang belanja telah dihapus');
            redirect(site_url('keranjang'), 'refresh');
        } else {
            // semua
            $this->cart->destroy();
            $this->session->set_flashdata('sukses', 'Data keranjang belanja telah dihapus');
            redirect(site_url('keranjang'), 'refresh');
        }
    }
}
