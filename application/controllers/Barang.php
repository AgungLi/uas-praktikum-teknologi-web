<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_barang');
    }
    //halaman utama website - homepage
    public function index()
    {
        $barang     = $this->model_barang->home();
        $data = array(
            'title'         => 'Data Barang',
            'barang'        => $barang,
            'isi'           => 'home/produk'
        );
        $this->load->view('layout/wrapper', $data, FALSE);
    }

    public function databarang()
    {
        $barang     = $this->model_barang->tampil_data();
        $data = array(
            'title'         => 'Data Barang',
            'barang'        => $barang,
            'isi'           => 'barang/list'
        );
        $this->load->view('layout/wrapper', $data, FALSE);
    }

    // //tambah produk
    public function tambah()
    {
        //validasi input
        $valid = $this->form_validation;
        $valid->set_rules(
            'nama_barang',
            'Nama Barang',
            'required',
            array(
                'required' => '%s harus diisi'
            )
        );
        $valid->set_rules(
            'harga_per_pcs',
            'Harga Barang',
            'required',
            array(
                'required' => '%s harus diisi'
            )
        );


        if ($valid->run()) {
            $i = $this->input;
            $data = array(
                'nama_barang'   => $i->post('nama_barang'),
                'stok_barang'   => $i->post('stok_barang'),
                'harga_per_pcs'   => $i->post('harga_per_pcs')
            );
            $this->model_barang->tambah($data);
            $this->session->set_flashdata('sukses', 'data telah ditambahkan');
            redirect(site_url('barang/databarang'), 'refresh');
        }
        // end masuk database
        $data = array(
            'title'     => 'Tambah produk',
            'isi'       => 'barang/tambah'
        );
        $this->load->view('layout/wrapper', $data, FALSE);
    }

    // edit
    public function edit($id_barang)
    {
        // ambil data produk
        $barang = $this->model_barang->detail($id_barang);
        //validasi input
        $valid = $this->form_validation;
        $valid->set_rules(
            'nama_barang',
            'Nama Barang',
            'required',
            array(
                'required' => '%s harus diisi'
            )
        );

        $valid->set_rules(
            'stok_barang',
            'Stok Barang',
            'required',
            array(
                'required' => '%s harus diisi'
            )
        );

        $valid->set_rules(
            'harga_per_pcs',
            'Harga',
            'required',
            array(
                'required' => '%s harus diisi'
            )
        );

        if ($valid->run()) {
            $i = $this->input;
            $data = array(
                'id_barang'   => $barang->id_barang,
                'nama_barang'   => $i->post('nama_barang'),
                'stok_barang'   => $i->post('stok_barang'),
                'harga_per_pcs'   => $i->post('harga_per_pcs')
            );
            $this->model_barang->edit($data);
            $this->session->set_flashdata('sukses', 'data telah diedit');
            redirect(site_url('barang/databarang'), 'refresh');
        }
        // end masuk database
        $data = array(
            'title'     => 'Edit produk: ' . $barang->nama_barang,
            'barang'    => $barang,
            'isi'       => 'barang/edit'
        );
        $this->load->view('layout/wrapper', $data, FALSE);
    }
    // delete produk
    public function delete($id_barang)
    {
        $data = array('id_barang' => $id_barang);
        $this->model_barang->delete($data);
        $this->session->set_flashdata('sukses', 'data telah dihapus');
        redirect(site_url('barang/databarang'), 'refresh');
    }
}
