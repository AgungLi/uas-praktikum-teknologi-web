<section class="hero-wrap hero-wrap-2" style="background-image: url(<?php echo base_url('assets/template/images/bg_3.jpeg') ?>);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate text-center mb-4">
                <h1 class="mb-2 bread"><?= $title; ?></h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Data Barang <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-cart">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <?php
                    if ($this->session->flashdata('sukses')) {
                        echo '<div class="alert alert-warning">';
                        echo $this->session->flashdata('sukses');
                        echo '</div>';
                    }
                    ?>
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th class="column-2">No</th>
                                <th class="column-3">Nama Barang</th>
                                <th class="column-4">Stock</th>
                                <th class="column-5">Harga</th>
                                <th class="column-6">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($barang as $barang) {
                            ?>
                                <tr class="table-row text-center">
                                    <td class="column-2">
                                        <h3><?php ?></h3>
                                    </td>
                                    <td class="column-3">
                                        <h3><?php echo $barang->nama_barang ?></h3>
                                    </td>
                                    <td class="column-4">
                                        <h3><?php echo $barang->stok_barang ?></h3>
                                    </td>
                                    <td class="column-5">Rp. <?php echo number_format($barang->harga_per_pcs, '0', ',', '.') ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?php echo site_url('barang/edit/' . $barang->id_barang) ?>" class="btn btn-dark btn-xs">
                                                <i class="fas fa-eye"></i> Edit
                                            </a>
                                            <a href="<?php echo site_url('barang/delete/' . $barang->id_barang) ?>" class="btn btn-primary btn-xs">
                                                <i class="fas fa-upload"></i> Hapus
                                            </a>
                                        </div>
                                    </td>
                                </tr><!-- END TR-->
                            <?php
                                echo form_close();
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <p class="fa-pull-right">
                    <a href="<?php echo site_url('barang/tambah') ?>" class="btn btn-dark btn-lg">
                        <i class="fas fa-trash"></i>
                        Tambah Barang
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>