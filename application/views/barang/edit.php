<section class="hero-wrap hero-wrap-2" style="background-image: url(<?php echo base_url('assets/template/images/bg_3.jpeg') ?>);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate text-center mb-4">
                <h1 class="mb-2 bread"><?= $title; ?></h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Produk <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>

<div class="col-lg-12 ftco-animate p-md-2">
    <div class="row">
        <div class="col-md-12 nav-link-wrap mb-5">
            <div class="nav ftco-animate nav-pills justify-content-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <?php
                //notifikasi error
                echo validation_errors('<div class="alert alert-warning">', '</div>');

                //form open
                echo form_open(site_url('barang/edit/' . $barang->id_barang), ' class="form-horizontal"');
                ?>
                <table class="table">
                    <tbody>
                        <tr>
                            <td width="30%">Nama Barang</td>
                            <td>
                                <input type="text" name="nama_barang" class="form-control" placeholder="Nama Barang" value="<?php echo $barang->nama_barang ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Stok Barang</td>
                            <td>
                                <input type="text" name="stok_barang" class="form-control" placeholder="Stok Barang" value="<?php echo $barang->stok_barang ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td>
                                <input type="text" name="harga_per_pcs" class="form-control" placeholder="Harga Per Pcs" value="<?php echo $barang->harga_per_pcs ?>">
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-dark btn-sm" type="submit" name="submit"><i class="fas fa-upload"></i> Simpan</button>
                                    <button class="btn btn-primary btn-sm" type="reset" name="reset"><i class="fas fa-times"></i> Reset</button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>