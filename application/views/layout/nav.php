<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <strong>UAS Praktikum Teknologi Web</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <!-- HOME -->
                <li class="nav-item"><a href="<?php echo base_url() ?>" class="nav-link"><strong>Dashboard</strong></a></li>
                <li class="nav-item"><a href="<?php echo site_url('barang/databarang') ?>" class="nav-link"><strong>Data Barang</strong></a></li>
                <!-- Nav Item - Alerts -->
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-cart-plus fa-fw"></i>
                        <!-- Counter - Alerts -->
                        <?php
                        // check data belanjaan ada atau tidak
                        $keranjang = $this->cart->contents();
                        ?>
                        <span class="badge badge-danger badge-counter"><?php echo count($keranjang) ?></span>
                    </a>
                    <!-- Dropdown - Alerts -->
                    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">

                        <?php
                        if (empty($keranjang)) {
                        ?>
                            <h6 class="dropdown-header">
                                <?php echo "Keranjang Kosong" ?>
                            </h6>
                        <?php
                        } else {
                        ?>
                            <h6 class="text-center">
                                <span class="font-weight-bold">Keranjang Belanja</span>
                            </h6>
                            <hr>
                            <?php
                            $total = 'Rp. ' . number_format($this->cart->total(), '0', ',', '.');
                            foreach ($keranjang as $keranjang) {
                                $id_barang = $keranjang['id'];
                                // ambil data produk
                                $produknya = $this->model_barang->detail($id_barang);
                            ?>
                                <a class="dropdown-item d-flex align-items-center" href="<?php /*echo base_url('produk/detail/' . $produknya->slug_produk)  */ ?>">
                                    <div class="mr-3">
                                        <!-- <img style="width: 50px" class="" src="<?php echo base_url('assets/upload/image/thumbs/') . $produknya->gambar ?>" alt="<?php echo $keranjang['name'] ?>"> -->
                                    </div>
                                    <div>
                                        <span class="font-weight-bold"><?php echo $keranjang['name'] ?></span>
                                        <div class="small text-gray-50 font-weight-bold"><?php echo $keranjang['qty'] ?> X Rp. <?php echo number_format($keranjang['price'], '0', ',', '.') ?></div>
                                    </div>
                                </a>
                            <?php
                            } // tutup foreach 
                            ?>
                            <div class="font-weight-bold text-center">
                                <hr>
                                Total : <?php echo $total ?>
                            </div>
                            <a class="dropdown-item text-center small text-white-500" href="<?php echo site_url('keranjang') ?>"> <span class="btn btn-dark btn-sm btn-block">Tampilkan Semua</span></a>
                        <?php
                        } // tutup if
                        ?>

                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo site_url('barang') ?>">
                        <span class="font-weight-bold"><strong>Agung Laksmana Ismail</strong></span>
                    </a>
                </li>

            </ul>


        </div>
    </div>
</nav>