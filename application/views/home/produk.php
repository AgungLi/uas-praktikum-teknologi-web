<section class="hero-wrap hero-wrap-2" style="background-image: url(<?php echo base_url('assets/template/images/bg_3.jpeg') ?>);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate text-center mb-4">
                <h1 class="mb-2 bread"><?= $title; ?></h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Dashboard <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>

</section>

<section class="ftco-section">
    <div class="container">
        <div class="ftco-search">
            <div class="row justify-content-center mb-5 pb-2">
                <div class="col-md-12 text-center heading-section ftco-animate">
                    <!-- <span class="subheading">SAE</span> -->
                    <h2 class="mb-4">Barang</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-15 tab-wrap">
                    <div class="tab-content" id="v-pills-tabContent">


                        <div class="tab-pane fade show active" id="" role="tabpanel" aria-labelledby="day-1-tab">
                            <div class="row text-center">
                                <?php foreach ($barang as $barang) { ?>
                                    <div class="card ml-3 mb-3" style="width: 16rem;">
                                        <?php
                                        // Form untuk memproses belanjaan
                                        echo form_open(site_url('keranjang/add'));
                                        // elemen yang dibawa
                                        echo form_hidden('id_barang', $barang->id_barang);
                                        echo form_hidden('jum_barang', 1);
                                        echo form_hidden('harga_per_pcs', $barang->harga_per_pcs);
                                        echo form_hidden('nama_barang', $barang->nama_barang);
                                        // Elemen redirect
                                        echo form_hidden('redirect_page', str_replace('index.php/', '', current_url()));
                                        ?>
                                        <div class="card-body">
                                            <h5 class="card-title mb-1"><?= $barang->nama_barang; ?></h5>
                                            <span class="badge badge-dark mb-3">IDR <?= number_format($barang->harga_per_pcs, '0', ',', '.'); ?></span>
                                            <div class="faded">
                                                <ul class="ftco-social">
                                                    <button type="submit" value="submit" class="btn btn-dark"><i class="fas fa-shopping-cart"></i></button>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                        // closing form
                                        echo form_close();
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>