<section class="hero-wrap hero-wrap-2" style="background-image: url(<?php echo base_url('assets/template/images/bg_3.jpeg') ?>);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate text-center mb-4">
                <h1 class="mb-2 bread"><?= $title; ?></h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Keranjang <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>

</section>

<section class="ftco-section ftco-cart">
    <div class="container">
        <div class="row">

            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <?php
                    if ($this->session->flashdata('sukses')) {
                        echo '<div class="alert alert-warning">';
                        echo $this->session->flashdata('sukses');
                        echo '</div>';
                    }
                    ?>
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th class="column-3">Produk</th>
                                <th class="column-4">Harga</th>
                                <th class="column-5">Jumlah</th>
                                <th class="column-6">Sub Total</th>
                                <th class="column-6">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            foreach ($keranjang as $keranjang) {
                                //ambil data produk
                                $id_barang = $keranjang['id'];
                                $produk = $this->model_barang->detail($id_barang);

                                // Form update keranjang
                                echo form_open(site_url('keranjang/update_cart/' . $keranjang['rowid']));

                            ?>
                                <tr class="table-row text-center">


                                    <td class="column-2">
                                        <h3><?php echo $keranjang['name'] ?></h3>
                                    </td>

                                    <td class="column-3">Rp. <?php echo number_format($keranjang['price'], '0', ',', '.') ?></td>

                                    <td class="column-4">
                                        <div class="form-group">
                                            <input type="text" name="jum_barang" class="col-md-4 text-center " value="<?php echo $keranjang['qty'] ?>" min="1" max="100">
                                        </div>
                                    </td>

                                    <td class="column-5">Rp.
                                        <?php
                                        $sub_total = $keranjang['price'] * $keranjang['qty'];
                                        echo number_format($sub_total, '0', ',', '.')
                                        ?>
                                    </td>
                                    <td>
                                        <button type="submit" name="update" class="btn btn-dark btn-sm">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                        <a href="<?php echo site_url('keranjang/hapus/' . $keranjang['rowid']) ?>" name="hapus" class="btn btn-dark btn-sm">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr><!-- END TR-->
                            <?php
                                echo form_close();
                            }
                            ?>
                            <tr class="table-row text-center">
                                <td colspan="4">
                                    <h4>Total Belanja</h4>
                                </td>
                                <td>Rp. <?php echo number_format($this->cart->total(), '0', ',', '.') ?></td>
                            </tr>
                        </tbody>

                    </table>

                </div>
                <p class="fa-pull-right">
                    <a href="<?php echo site_url('keranjang/hapus') ?>" class="btn btn-dark btn-lg">
                        <i class="fas fa-trash"></i>
                        Bersihkan
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>